/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.oxgui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;



/**
 *
 * @author gamme
 */
public class TestReadPlayer {
    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException  {
        File file  = new File("player.dat");
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Player o = (Player) ois.readObject();
        Player x = (Player) ois.readObject();
        ois.close();
        fis.close();
        System.out.println(o);
        System.out.println(x);
    }
}
